#include "velocity_controller.h"

VelocityController::VelocityController(const string &urdf_path)
{
  setUrdf_path(urdf_path);
}

VelocityController::~VelocityController()
{
  // Destructor
}

vector<double> VelocityController::calcJointVelocities(geometry_msgs::Pose goal_pose, vector<double> joint_angles)
{

}

bool VelocityController::moveTo(geometry_msgs::Pose pose_wrt_base)
{

}

bool VelocityController::moveToNeutral()
{

}

bool VelocityController::moveToZero()
{

}

bool VelocityController::isAtGoalPose(geometry_msgs::Pose pose_wrt_base)
{

}

bool VelocityController::isAtSingularity()
{

}

geometry_msgs::Pose VelocityController::calcEndEffectorPose(vector<double> joint_angles)
{

}

geometry_msgs::Twist VelocityController::calcEndEffectorVelocity(geometry_msgs::Pose goal_pose, geometry_msgs::Pose start_pose)
{

}

geometry_msgs::Twist VelocityController::scaleVelocity(geometry_msgs::Twist)
{

}

vector<double> VelocityController::scaleVelocity(vector<double> joint_angles)
{

}

double VelocityController::getSingularity_threshold() const
{
  return singularity_threshold_;
}

void VelocityController::setSingularity_threshold(double singularity_threshold)
{
  singularity_threshold_ = singularity_threshold;
}

vector<double> VelocityController::getNeutral_pose() const
{
  return neutral_pose_;
}

void VelocityController::setNeutral_pose(const vector<double> &neutral_pose)
{
  neutral_pose_ = neutral_pose;
}

vector<double> VelocityController::getVmax_joint() const
{
  return vmax_joint_;
}

void VelocityController::setVmax_joint(const vector<double> &vmax_joint)
{
  vmax_joint_ = vmax_joint;
}

double VelocityController::getVmax_ee_y() const
{
  return vmax_ee_y_;
}

void VelocityController::setVmax_ee_y(double vmax_ee_y)
{
  vmax_ee_y_ = vmax_ee_y;
}

double VelocityController::getVmax_ee_p() const
{
  return vmax_ee_p_;
}

void VelocityController::setVmax_ee_p(double vmax_ee_p)
{
  vmax_ee_p_ = vmax_ee_p;
}

double VelocityController::getVmax_ee_r() const
{
  return vmax_ee_r_;
}

void VelocityController::setVmax_ee_r(double vmax_ee_r)
{
  vmax_ee_r_ = vmax_ee_r;
}

double VelocityController::getVmax_ee_tz() const
{
  return vmax_ee_tz_;
}

void VelocityController::setVmax_ee_tz(double vmax_ee_tz)
{
  vmax_ee_tz_ = vmax_ee_tz;
}

double VelocityController::getVmax_ee_ty() const
{
  return vmax_ee_ty_;
}

void VelocityController::setVmax_ee_ty(double vmax_ee_ty)
{
  vmax_ee_ty_ = vmax_ee_ty;
}

double VelocityController::getVmax_ee_tx() const
{
  return vmax_ee_tx_;
}

void VelocityController::setVmax_ee_tx(double vmax_ee_tx)
{
  vmax_ee_tx_ = vmax_ee_tx;
}

string VelocityController::getUrdf_path() const
{
  return urdf_path_;
}

void VelocityController::setUrdf_path(const string &urdf_path)
{
  urdf_path_ = urdf_path;
}

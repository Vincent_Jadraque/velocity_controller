/*
 * Copyright (c) 2019, Vincent Jadraque
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *   * Redistributions of source code must retain the above copyright notice, this
 *     list of conditions and the following disclaimer.
 *
 *   * Redistributions in binary form must reproduce the above copyright notice,
 *     this list of conditions and the following disclaimer in the documentation
 *     and/or other materials provided with the distribution.
 *
 *   * Neither the name of roscpp_code_format nor the names of its
 *     contributors may be used to endorse or promote products derived from
 *     this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef VELOCITY_CONTROLLER_H
#define VELOCITY_CONTROLLER_H

#include "ros/ros.h"
#include <Eigen/Dense>
#include <geometry_msgs/Twist.h>
#include <geometry_msgs/Pose.h>
#include <kdl_parser/kdl_parser.hpp>
#include <kdl/chainiksolvervel_pinv.hpp>

using std::string;
using std::vector;
using geometry_msgs::Twist;
using geometry_msgs::Pose;

class VelocityController
{
public:
  VelocityController(const std::string &urdf_path);
  ~VelocityController();

  // calculate the joint velocities from given end effector velocity
  vector<double> calcJointVelocities(Pose goal_pose, vector<double> joint_angles);

  // move to 3D pose wrt base
  bool moveTo(Pose pose_wrt_base);

  // move arm to neutral position
  bool moveToNeutral();

  // move arm to position to joint angles at 0
  bool moveToZero();

  // check if goal pose has been reached
  bool isAtGoalPose(Pose pose_wrt_base);

  // check for singularity
  bool isAtSingularity();

  // member getter and setters
  string getUrdf_path() const;
  void setUrdf_path(const string &urdf_path);

  double getVmax_ee_tx() const;
  void setVmax_ee_tx(double vmax_ee_tx);

  double getVmax_ee_ty() const;
  void setVmax_ee_ty(double vmax_ee_ty);

  double getVmax_ee_tz() const;
  void setVmax_ee_tz(double vmax_ee_tz);

  double getVmax_ee_r() const;
  void setVmax_ee_r(double vmax_ee_r);

  double getVmax_ee_p() const;
  void setVmax_ee_p(double vmax_ee_p);

  double getVmax_ee_y() const;
  void setVmax_ee_y(double vmax_ee_y);

  vector<double> getVmax_joint() const;
  void setVmax_joint(const vector<double> &vmax_joint);

  vector<double> getNeutral_pose() const;
  void setNeutral_pose(const vector<double> &neutral_pose);

  double getSingularity_threshold() const;
  void setSingularity_threshold(double singularity_threshold);

private:
  // calculate the end effector pose given the joint angles
  Pose calcEndEffectorPose(vector<double> joint_angles);

  // Calculate the end effector velocity from a given goal pose and starting pose
  Twist calcEndEffectorVelocity(Pose goal_pose, Pose start_pose);

  // scale velocity of end effector to within safe limits
  Twist scaleVelocity(Twist);

  // overload scale velocity for joint angles
  vector<double> scaleVelocity(vector<double> joint_angles);

  //! \var urdf file path
  string urdf_path_;

  //! \var double vmax of end effector translation x,y,z, and roll, pitch, yaw
  double vmax_ee_tx_;
  double vmax_ee_ty_;
  double vmax_ee_tz_;
  double vmax_ee_r_;
  double vmax_ee_p_;
  double vmax_ee_y_;

  //! \var vmax for each joint
  vector<double> vmax_joint_;

  //! \var joint angles for neutral pose
  vector<double> neutral_pose_;

  //! \var singularity threshold
  double singularity_threshold_;
};

#endif // VELOCITY_CONTROLLER_H
